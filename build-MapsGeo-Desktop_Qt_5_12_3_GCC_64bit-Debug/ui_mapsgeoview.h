/********************************************************************************
** Form generated from reading UI file 'mapsgeoview.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAPSGEOVIEW_H
#define UI_MAPSGEOVIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MapsGeoView
{
public:
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MapsGeoView)
    {
        if (MapsGeoView->objectName().isEmpty())
            MapsGeoView->setObjectName(QString::fromUtf8("MapsGeoView"));
        MapsGeoView->resize(1024, 768);
        centralWidget = new QWidget(MapsGeoView);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        MapsGeoView->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MapsGeoView);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1024, 22));
        MapsGeoView->setMenuBar(menuBar);
        statusBar = new QStatusBar(MapsGeoView);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MapsGeoView->setStatusBar(statusBar);

        retranslateUi(MapsGeoView);

        QMetaObject::connectSlotsByName(MapsGeoView);
    } // setupUi

    void retranslateUi(QMainWindow *MapsGeoView)
    {
        MapsGeoView->setWindowTitle(QApplication::translate("MapsGeoView", "MapsGeoView", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MapsGeoView: public Ui_MapsGeoView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAPSGEOVIEW_H
