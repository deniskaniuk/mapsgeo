#include "tilesloader.h"

TilesLoader::TilesLoader(QObject *parent) : QObject(parent)
{

}

QVector2D TilesLoader::GetCurrentTileName(double latitude, double longitude, int zoom)
{
    QVector2D coordinates;

    int x, y = 0;
    x = (int)(floor((longitude + 180.0) / 360.0 * (1 << zoom)));

    double latrad = latitude * M_PI/180.0;
    y = (int)(floor((1.0 - asinh(tan(latrad)) / M_PI) / 2.0 * (1 << zoom)));

    coordinates.setX(x);
    coordinates.setY(y);

    return coordinates;

}
