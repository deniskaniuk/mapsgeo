#ifndef MAPSGEOVIEW_H
#define MAPSGEOVIEW_H

#include <QMainWindow>
#include <tilesloader.h>

#include <QDebug>

namespace Ui {
class MapsGeoView;
}

class MapsGeoView : public QMainWindow
{
    Q_OBJECT

public:
    explicit MapsGeoView(QWidget *parent = nullptr);
    ~MapsGeoView();

private:
    Ui::MapsGeoView *ui;

    TilesLoader *m_Tiles;

    void LoadCentralTile(int xtile, int ytile, int zoom);

};

#endif // MAPSGEOVIEW_H
