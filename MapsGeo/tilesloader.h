#ifndef TILESLOADER_H
#define TILESLOADER_H

#include <QObject>

#include <QVector2D>
#include <tgmath.h>

class TilesLoader : public QObject
{
    Q_OBJECT
public:
    explicit TilesLoader(QObject *parent = nullptr);

    QVector2D GetCurrentTileName(double latitude, double longitude, int zoom);

signals:

public slots:
};

#endif // TILESLOADER_H
