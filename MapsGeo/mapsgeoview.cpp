#include "mapsgeoview.h"
#include "ui_mapsgeoview.h"

MapsGeoView::MapsGeoView(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MapsGeoView)
{
    ui->setupUi(this);

    m_Tiles = new TilesLoader;


    //test example---------------
    double latitude = 53.9299;      //Minsk coordinates
    double longitude = 27.5109;
    int zoom = 2;
    //---------------------------

    QVector2D tilesNumber(m_Tiles->GetCurrentTileName(latitude, longitude, zoom));

    qDebug()<<tilesNumber;

    LoadCentralTile(tilesNumber.x(), tilesNumber.y(), zoom);
}

MapsGeoView::~MapsGeoView()
{
    if(m_Tiles)
        delete m_Tiles;


    delete ui;
}

void MapsGeoView::LoadCentralTile(int xtile, int ytile, int zoom)
{

}
